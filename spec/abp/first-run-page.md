# First Run Page

The First Run page will be shown to all Adblock Plus desktop users following a successful installation of the extension. This page will open in a new tab and is a one-time event.

## Index

1. [General requirements](#general-requirements)
1. [Warnings](#warnings)
1. [Assets](#assets)


## Navigation

### Header

There will be no primary navigation menu items in the header, other than the logo, which redirects to the adblockplus.org index page. 

### Footer

`Copyright © 2018 eyeo GmbH. All rights reserved. Adblock Plus® is a registered trademark of [eyeo GmbH][1].

[1] eyeo redirect to eyeo.com

## Index

![](/res/abp/first-run-page/abp-first-run-page.png)

```
 # INSTALLATION SUCCESSFUL!

You just took control of your browser

 ## YOU'RE IN CONTROL

By default, you may see some nonintrusive ads that adhere to [strict criteria][0]. We identify these ads as Acceptable Ads. Prefer to block all ads? [Turn off Acceptable Ads][1] in your [Settings][2].

 ## ADBLOCK BROWSER APP

From the team behind Adblock Plus, the most popular ad blocker for desktop browsers, Adblock Browser is now available for your Android and iOS devices.

 ## FAIR USE

We encourage you to make fair use of Adblock Plus and the option to whitelist websites. Depending on the filters you use, you understand and accept that unintentional results might occur (e.g. blocked content on a website). Refer to the [Terms of Use][3] for more information about fair use of Adblock Plus.
```

[0]: Opens Documentation link: %LINK%=acceptable_ads_criteria in a new tab

[1]: Opens Documentation link: %LINK%=acceptable_ads_opt_out in a new tab

[2]: Opens: Settings menu in a new tab

[3]: Opens Documentation link: %LINK%=terms in a new tab

#### App store links

| App store | Link | alt text |
| --- | ---- | --- | 
| iTunes |  %LINK%=adblock_browser_ios_store | Available on the iTunes App Store |
| Google Play |  %LINK%=adblock_browser_android_store | Android app on Google Play |

#### Responsiveness

- Optimize for a screen widths of 1200px, 992px, 768px and 576px.
- Readable text area on large screens should not extend beyond 1024px in width.
- Spacing between buttons should become wider as the column width increases, and should stack as the space decreases.
- It is important that the text in the first column appears above the fold. 

#### Link behaviour 

All external links, including links to adblockplus.org should open in a new tab.

## Warning

![](/res/abp/first-run-page/abp-first-run-page-error.png)

When an error message is shown, then hide the headline text `# INSTALLATION SUCCESSFUL! You just took control of your browser`.

If there are more than one warning messages - only display the [data corruption warning](#data-corruption-warning).

### Filter list warning

`It seems that an issue caused all filters to be removed and we were unable to restore a backup. Therefore we had to reset your filters and Acceptable Ads settings. Please check your filter lists and Acceptable Ads settings in the [Adblock Plus options](Open Settings Page in a new tab).`

### Data corruption warning 

If the first run page will be shown and the data corruption flag is set show the below text.

#### Explanation text

Explanation text that informs the user that his settings storage has been corrupted and that he needs to reinstall the extension.

```
Some users are experiencing an issue where this page opens each time their browser is launched. This issue can be resolved by uninstalling and reinstalling Adblock Plus:

1. Open your browser's Extensions tab. 
2. Locate Adblock Plus and remove it.
3. Reinstall Adblock Plus from [adblockplus.org][0].
```

[0]: Opens Documentation link: %LINK%=adblock_plus in a new tab.

#### User support text

Text in smaller font size to not distract from the Explanation text that tells the user to contact user support.

`If you still need help, contact us at [support@adblockplus.org][0] or visit our [Help Center][1].`

[0]: support@adblockplus.org

[1]: Opens Documentation link: %LINK%=help_center in a new tab.


## Assets

| Name | File |
| ---- | ---- |
| abp-logo.svg | ![](/res/abp/first-run-page/assets/abp-logo.svg) |
| checkmark-header.svg | ![](/res/abp/first-run-page/assets/checkmark-header.svg) |
| checkmark.svg | ![](/res/abp/first-run-page/assets/checkmark.svg) |
| rocket.svg | ![](/res/abp/first-run-page/assets/rocket.svg) |
| lock.svg | ![](/res/abp/first-run-page/assets/lock.svg) |
| googleplay-bg.svg | ![](/res/abp/first-run-page/assets/googleplay-bg.svg) |
| appstore-bg.svg | ![](/res/abp/first-run-page/assets/appstore-bg.svg) |
| footer-facebook-glyphicon.png | ![](/res/abp/first-run-page/assets/footer-facebook-glyphicon.png) |
| footer-instagram-glyphicon.png | ![](/res/abp/first-run-page/assets/footer-instagram-glyphicon.png) |
| footer-twitter-glyphicon.png | ![](/res/abp/first-run-page/assets/footer-twitter-glyphicon.png) |
| footer-youtube-glyphicon.png | ![](/res/abp/first-run-page/assets/footer-youtube-glyphicon.png) |
